/*
    Getting started with SDL2 - Just a Window.
*/

//For exit()
#include <stdlib.h>

//for printf (error message)
#include <stdio.h>

#if defined(_WIN32) || defined(_WIN64)
   //Include SDL library
    #include "SDL.h"
    //include SLD_Image, for support of loading diffrent images
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_Image.h"
#endif

SDL_Texture* load_texture(const char* file, SDL_Renderer* GameRenderer)
{
    SDL_Surface* tmp = IMG_Load(file);

    if (!tmp)
    {
        printf("Could not load image: %s: %s\n", file, SDL_GetError());
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(GameRenderer, tmp);

    if (!texture)
    {
        printf("Could not create texture from surface: %s: %s\n", file, SDL_GetError());
    }

    SDL_FreeSurface(tmp);
    tmp = nullptr;

    return texture;
}

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

const int SDL_OK = 0;

int main(int argc, char* args[])
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;
    
    //texutre variables
    SDL_Surface* temp = nullptr;
    
    //texture varibles which store actual sprites. (will be optimed)
    SDL_Texture* bgTexture = nullptr;
    SDL_Texture* playerTexture = nullptr;

    //player image - source rectangle
    SDL_Rect SourceRect;
    SDL_Rect TargetRect;

    //angle & flipping
    float angle;
    SDL_RendererFlip flip;

    SourceRect.x = 0;
    SourceRect.y = 0;

    TargetRect.x = 100;
    TargetRect.y = 400;

    flip = static_cast<SDL_RendererFlip>(SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);
    //set angle
    angle = 45.0f;

    const int SDL_OK = 0;

    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);
    
    //error management
    if (sdl_status != SDL_OK)
    {
       printf("Error - SDL Initalisation Failed\n");
       exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",          // Window title
        SDL_WINDOWPOS_UNDEFINED,  // X position
        SDL_WINDOWPOS_UNDEFINED,  // Y position
        800, 600,                 // width, height
        SDL_WINDOW_SHOWN);        // Window flags
    
    if (gameWindow != nullptr)
    {
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if (gameRenderer == nullptr)
        {
            //if the window creation succeded create out renderer
            printf("Error - SDL could not create renderer\n");
            exit(1);
        }
    }
    else
    {
        //could not create the window
        //so do not try create the renderer
        printf("Error - SDL could not create window\n");
        exit(1);
    }

    bgTexture = load_texture("assets/images/background.png", gameRenderer);
    playerTexture = load_texture("assets/images/walker1.png", gameRenderer);

    SDL_QueryTexture(playerTexture, 0, 0, &(SourceRect.w), &(SourceRect.h));
    //scale and size
    TargetRect.w = SourceRect.w * 1.0f;
    TargetRect.h = SourceRect.h * 1.0f;

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
    SDL_RenderClear(gameRenderer);

    // 2. Draw the game objects
    SDL_RenderCopy(gameRenderer, bgTexture, NULL, NULL);
    SDL_RenderCopyEx(gameRenderer, playerTexture, &SourceRect, &TargetRect, angle, NULL, flip);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay(10000);

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;
    SDL_DestroyTexture(bgTexture);
    bgTexture = nullptr;
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;

    //Shutdown SDL - clear up resources etc. 
    SDL_Quit();

    exit(0);
}